package com.example.cartesian;

import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;

import java.util.Collections;
import java.util.List;

class CartesianProductContext implements TestTemplateInvocationContext {

  private final List<?> parameters;

  CartesianProductContext(List<?> parameters) {
    this.parameters = parameters;
  }

  @Override
  public String getDisplayName(int invocationIndex) {
    return invocationIndex + ": " + parameters;
  }

  @Override
  public List<Extension> getAdditionalExtensions() {
    return Collections.singletonList(new CartesianProductResolver(parameters));
  }
}
