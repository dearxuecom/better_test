package com.example.cartesian;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.util.List;

class CartesianProductResolver implements ParameterResolver {

  private final List<?> parameters;

  CartesianProductResolver(List<?> parameters) {
    this.parameters = parameters;
  }

  @Override
  public boolean supportsParameter(
      ParameterContext parameterContext, ExtensionContext extensionContext) {
    return parameterContext.getIndex() < parameters.size();
  }

  @Override
  public Object resolveParameter(
      ParameterContext parameterContext, ExtensionContext extensionContext) {
    return parameters.get(parameterContext.getIndex());
  }
}
