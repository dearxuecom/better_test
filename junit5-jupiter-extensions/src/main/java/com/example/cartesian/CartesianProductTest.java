package com.example.cartesian;

import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@TestTemplate
@ExtendWith(CartesianProductProvider.class)
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CartesianProductTest {

  String[] value() default {};

  class Sets {

    private final List<List<?>> sets = new ArrayList<>();

    public Sets add(Object... entries) {
      sets.add(new ArrayList<>(Arrays.asList(entries)));
      return this;
    }

    List<List<?>> getSets() {
      return sets;
    }
  }
}
