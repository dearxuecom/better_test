package com.dearxue.tester.advanced;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class TempDirTest {

  @Test
  void tempDirectoryTestOne(@TempDir Path tempDir) throws IOException {
    Path tempFile = tempDir.resolve("test.txt");

    List<String> lines = Arrays.asList("dearxue.com");

    Files.write(tempFile, Arrays.asList("dearxue.com"));

    Assertions.assertTrue(Files.exists(tempFile), "Temp File should have been created");
    Assertions.assertEquals(Files.readAllLines(tempFile), lines);
  }
}
