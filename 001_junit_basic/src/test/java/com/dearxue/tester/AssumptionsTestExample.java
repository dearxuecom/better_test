package com.dearxue.tester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.*;

public class AssumptionsTestExample {

  @Test
  void testOnDev() {
    System.setProperty("ENV", "DEV");
    assumeTrue("DEV".equals(System.getProperty("ENV")));
    System.out.println("测试继续执行...");
  }
  /** 测试失败，打印出失败消息 */
  @Test
  void testOnProd() {
    System.setProperty("ENV", "PROD");
    assumeTrue("DEV".equals(System.getProperty("ENV")), AssumptionsTestExample::message);
    System.out.println("测试不会继续执行，不会打印此行...");
  }

  private static String message() {
    return "测试失败...";
  }

  @Test
  void trueAssumption() {
    assumeTrue(5 > 1);
    assertEquals(5 + 2, 7);
  }

  @Test
  void falseAssumption() {
    assumeFalse(5 < 1);
    assertEquals(5 + 2, 7);
  }

  @Test
  void assumptionThat() {
    String someString = "Just a string";
    assumingThat(someString.equals("Just a string"), () -> assertEquals(2 + 2, 4));
  }

  @Test
  void testInAllEnvironments() {
    System.setProperty("ENV", "DEV");
    assumingThat(
        "DEV".equals(System.getProperty("ENV")),
        () -> {
          // 仅在DEV服务器上执行这些断言和打印
          // 即System.setProperty("ENV", "DEV")才会执行
          assertEquals(3, 1 + 2);
        });

    // 在所有环境中执行这些断言
    assertEquals(13, 6 + 7);
  }
}
