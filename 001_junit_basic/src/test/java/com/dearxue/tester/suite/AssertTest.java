package com.dearxue.tester.suite;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

public class AssertTest {
  // 执行了
  @Test
  @Tag("DEV")
  void assertTest() {
    // 测试通过
    Assertions.assertNotEquals(3, 2 + 2);
    // 测试失败
    Assertions.assertNotEquals(4, 2 + 2, "Calculator.add(2, 2) test failed");
    // 测试失败
    Supplier<String> messageSupplier = () -> "Calculator.add(2, 2) test failed";
    Assertions.assertNotEquals(4, 2 + 2, messageSupplier);
  }
}
