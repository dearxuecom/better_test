package com.dearxue.tester.suite;

import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

// 将LifeCycleTests和AssertTest这两个类合并在一个分组中进行测试
@SelectClasses({LifeCycleTests.class, AssertTest.class})
// 也可以直接将多个包合并一个组中测试
// @SelectPackages({"com.dearxue.tester.suite"})
// 这个注解代表只测试含有DEV标签的测试类或者方法
@IncludeTags("PROD")
@Suite
@SuiteDisplayName("测试套件，分组测试")
public class SuiteTests {
  /**
   * <code>
   *     useJUnitPlatform {
   *         // includeTags 'fast'
   *         // excludeTags 'slow'
   *     }
   *
   * </code>
   */
}
