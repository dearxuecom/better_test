package com.dearxue.tester.suite;

import org.junit.jupiter.api.*;

public class LifeCycleTests {

  // 执行了
  @Tag("DEV")
  @DisplayName("生命周期测试")
  @Test
  void testCalcOne() {
    System.out.println("======测试1执行了=======");
    Assertions.assertEquals(4, 2 + 2);
  }
  // 执行了，但是如果SuiteTests类有@IncludeTags("DEV")注解则不会执行
  @Tag("PROD")
  @Test
  void testCalcThree() {
    System.out.println("======测试3执行了=======");
    Assertions.assertEquals(6, 2 + 4);
  }
  // 不会执行，因为被禁用了
  @Disabled
  @Test
  void testCalcTwo() {
    System.out.println("======测试2执行了=======");
    Assertions.assertEquals(6, 2 + 4);
  }
}
