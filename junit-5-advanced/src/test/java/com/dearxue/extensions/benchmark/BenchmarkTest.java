package com.dearxue.extensions.benchmark;

import org.junit.jupiter.api.Test;

@Benchmark
public class BenchmarkTest {

  @Test
  @Benchmark
  public void test_1() {
    try {
      Thread.sleep(2_000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void test_2() {
    try {
      Thread.sleep(1_000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
