package com.dearxue.junit5.mockito.repository;

public interface SettingRepository {

  int getUserMinAge();

  int getUserNameMinLength();
}
