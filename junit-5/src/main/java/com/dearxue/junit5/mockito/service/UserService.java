package com.dearxue.junit5.mockito.service;

import com.dearxue.junit5.mockito.User;

public interface UserService {

  User register(User user);
}
