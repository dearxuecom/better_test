package com.dearxue.junit5.mockito.repository;

import com.dearxue.junit5.mockito.User;

public interface MailClient {

  void sendUserRegistrationMail(User user);
}
